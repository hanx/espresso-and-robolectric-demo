package com.example.calculator;

import android.app.AlertDialog;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class RobolectricTest {

    MainActivity mainActivity;
    EditText txtNumber1;
    EditText txtNumber2;
    TextView txtResult;
    Button btnAdd;
    Button btnSubtract;
    Button btnMultiply;
    Button btnDivide;

    @Before
    public void before() {
        mainActivity = Robolectric.setupActivity(MainActivity.class);
        txtNumber1 = (EditText) mainActivity.findViewById(R.id.txtNumber1);
        txtNumber2 = (EditText) mainActivity.findViewById(R.id.txtNumber2);
        txtResult = (TextView) mainActivity.findViewById(R.id.txtResult);
        btnAdd = (Button) mainActivity.findViewById(R.id.btnAdd);
        btnSubtract = (Button) mainActivity.findViewById(R.id.btnSubtract);
        btnMultiply = (Button) mainActivity.findViewById(R.id.btnMultiply);
        btnDivide = (Button) mainActivity.findViewById(R.id.btnDivide);
    }

    // Validate Addition feature with valid input data
    @Test
    public void validateAddSuccess() {
        txtNumber1.setText("1");
        txtNumber2.setText("2");
        btnAdd.performClick();

        assertEquals(txtResult.getText().toString(), "3");
    }

    // Validate Subtraction feature with valid input data
    @Test
    public void validateSubtractSuccess() {
        txtNumber1.setText("2");
        txtNumber2.setText("1");
        btnSubtract.performClick();

        assertEquals(txtResult.getText().toString(), "1");
    }

    // Validate Multiplication feature with valid input data
    @Test
    public void validateMultiplySuccess() {
        txtNumber1.setText("2");
        txtNumber2.setText("2");
        btnMultiply.performClick();

        assertEquals(txtResult.getText().toString(), "4");
    }

    // Validate Division feature with valid input data
    @Test
    public void validateDivideSuccess() {
        txtNumber1.setText("2");
        txtNumber2.setText("2");
        btnDivide.performClick();

        assertEquals(txtResult.getText().toString(), "1");
    }

    // Validate Addition feature with invalid input data
    @Test
    public void validateAddWithOutOfInt() {
        txtNumber1.setText("12345678901");
        txtNumber2.setText("12345678901");
        btnDivide.performClick();

        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        assertEquals(Shadows.shadowOf(alertDialog).getMessage(), mainActivity.DLG_MESSAGE);
    }

    // Validate Division feature with invalid input data
    @Test
    public void validateDivideByZero() {
        txtNumber1.setText("2");
        txtNumber2.setText("0");
        btnDivide.performClick();

        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        assertEquals(Shadows.shadowOf(alertDialog).getMessage(), mainActivity.DLG_MESSAGE);
    }
}
