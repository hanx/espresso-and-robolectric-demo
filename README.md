This is a small test demo which uses Android Espresso and Robolectric test automation frameworks. The tests were written for testing a sample app which allows to perform mathematical operations such as addition, subtraction, multiplication and division of any two integer numbers.  

Before run this demo:
- Your machine should have Git and Android SDK installed.

To run this demo:
- Create a folder and open Terminal at the folder.
- Clone this project by executing the following command: 
```
git clone https://gitlab.com/sixthgearstudios-public/espresso-and-robolectric-demo
```

- Test Execution: 
   - Espresso tests:
      - Execute the following command: `./gradlew cAT`
      - After finishing the test, Test Report would be found at:
         - HTML test result files: <path_to_your_project>/app/build/outputs/reports/androidTests/connected/ directory.
         - XML test result files: <path_to_your_project>/app/build/outputs/androidTest-results/connected/ directory.
   - Robolectric tests:
      - Execute the following command: `./gradlew test`
      - After finishing the test, Test Report would be found at:
         - HTML test result files: <path_to_your_project>/app/build/reports/tests/ directory.
         - XML test result files: <path_to_your_project>/app/build/test-results/
